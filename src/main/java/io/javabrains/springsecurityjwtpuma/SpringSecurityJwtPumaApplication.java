package io.javabrains.springsecurityjwtpuma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityJwtPumaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityJwtPumaApplication.class, args);
	}

}
