package io.javabrains.springsecurityjwtpuma.controlador;

import io.javabrains.springsecurityjwtpuma.Entidad.RespuestaAutenticacion;
import io.javabrains.springsecurityjwtpuma.Entidad.SolicitudAutenticacion;
import io.javabrains.springsecurityjwtpuma.service.MiDetalleUsuarioServicio;
import io.javabrains.springsecurityjwtpuma.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class MascotaControlador {


    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MiDetalleUsuarioServicio miDetalleUsuarioServicio;

    @Autowired
    private JwtUtil jwtTokenUtil;


    @GetMapping(value = "/mascota")
    public String obtenerMascotas() {
        return "LOLO - CAMILO - LUNA - NINA - BOBY";
    }

    @PostMapping(value = "/autenticacion")
    public ResponseEntity<?> crearAutenticacionToken(@Valid @RequestBody SolicitudAutenticacion solicitudAutenticacion) throws Exception {

        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(solicitudAutenticacion.getUsuario(), solicitudAutenticacion.getContrasena()));

        } catch (BadCredentialsException e) {
            throw new Exception("Incorrecto en el usuario o contraseña");
        }
        final UserDetails userDetails = miDetalleUsuarioServicio.loadUserByUsername(solicitudAutenticacion.getUsuario());

        final String jwt = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new RespuestaAutenticacion(jwt));
    }
}
